<?php

declare(strict_types=1);

namespace Functional\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegisterUserControllerTest extends WebTestCase
{
    private const ENDPOINT = '/api/v1/users/register';

    public function setUp(): void 
    {
        parent::setUp();
    }

    /**
     * Comprueba registro, llega código 201 si el recurso se ha creado
     *
     * @return void
     */
    public function testRegisterUser(): void
    {
        $payload = [
            'name' => 'tesstPassed',
            'email' => 'hola@mundo.com',
            'password' => 'supersecret'
        ];

        $client = static::createClient();

        $client->request('POST', self::ENDPOINT, [], [], [], \json_encode($payload));

        $response = $client->getResponse();

        // Comprueba que la página es accesible si nos devuelve un cod. 200
        $this->assertResponseStatusCodeSame(201);
        self::assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
    }

    /** @test 
     *
     * Comprueba registro, llega código 201 si el recurso se ha creado
     *
     * @return void
    */
    public function testRegisterUserWithNoName(): void
    {
        $payload = [
            'name' => '',
            'email' => 'hola@mundo.com',
            'password' => 'supersecret'
        ];

        $client = static::createClient();

        $client->request('POST', self::ENDPOINT, [], [], [], json_encode($payload));
        
        $response = $client->getResponse();

        // Comprueba que un payload sin el nombre nos devuelve un cod 400 : BAD_REQUEST
        $this->assertResponseStatusCodeSame(
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    public function testRegisterUserWithNoMail(): void
    {
        $payload = [
            'name' => 'funkopop',
            'email' => '',
            'password' => 'supersecret'
        ];

        $client = static::createClient();

        $client->request('POST', self::ENDPOINT, [], [], [], json_encode($payload));
        
        $response = $client->getResponse();

        // Comprueba que un payload sin el nombre nos devuelve un cod 400 : BAD_REQUEST
        $this->assertResponseStatusCodeSame(
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    public function testRegisterUserWithNoPassword(): void
    {
        $payload = [
            'name' => 'funkopop',
            'email' => 'hola@mundo.com',
            'password' => ''
        ];

        $client = static::createClient();

        $client->request('POST', self::ENDPOINT, [], [], [], json_encode($payload));
        
        $response = $client->getResponse();

        // Comprueba que un payload sin el nombre nos devuelve un cod 400 : BAD_REQUEST
        $this->assertResponseStatusCodeSame(
            JsonResponse::HTTP_BAD_REQUEST
        );
    }
    
}