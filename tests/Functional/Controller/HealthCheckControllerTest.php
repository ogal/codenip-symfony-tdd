<?php

declare(strict_types=1);

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class HealthCheckControllerTest extends WebTestCase
{
    private const ENDPOINT = '/api/v1/health-check';

    public function testHealthCheck(): void 
    {
        $client = static::createClient();

        $client->request('GET', self::ENDPOINT);

        $response = $client->getResponse();

        // Comprueba que la página es accesible si nos devuelve un cod. 200
        $this->assertResponseIsSuccessful();
        self::assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode(), 'Test superado!');
    }
}