<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HealthCheckController
{
    /**
     * Devuelve un response vacío
     *
     * @param Request $request
     * @return void
     */
    public function __invoke(Request $request)
    {
        return new JsonResponse();
    }
}