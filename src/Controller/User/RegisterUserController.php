<?php

declare(strict_types=1);

namespace App\Controller\User;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RegisterUserController
{
    
    public function __invoke(Request $request): JsonResponse
    {
        $payload = json_decode($request->getContent(), true);

        dump($payload);

        if (count($payload)==0) {
            dump('No hay ningun payload');
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }
        else
        {

            foreach ($payload as $key => $value) {
                
                if(empty($value))
                {
                    /** Ambas opciones son validas */
                    // return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
                    throw new BadRequestHttpException('Param.: ' . $key . ' => No puede quedar vacío');
                }
            }

            return new JsonResponse(null, JsonResponse::HTTP_CREATED);
        }
    }
    
}

